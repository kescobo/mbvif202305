# Gut-resident microorganisms and their genes are associated with neuroanatomy and cognitive development

Presentation for Microbiome Virtual International Forum n.19, May 17th, 2023

by Kevin Bonham

[![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline-status/kescobo/mbvif202305?branch=main&style=for-the-badge)](https://gitlab.com/kescobo/mbvif202305/-/commits/main)
[![Download PDF icon](https://img.shields.io/badge/Download-PDF-red?logo=adobeacrobatreader&style=for-the-badge)](https://gitlab.com/kescobo/mbvif202305/-/jobs/artifacts/main/raw/mbvif202305.pdf?job=pdf)

![Title slide of presentation](assets/title-slide.png)
