\documentclass[10pt,aspectratio=169]{beamer}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{appendixnumberbeamer}
\usepackage{graphicx}
\graphicspath{{assets/}}
\usepackage[none]{hyphenat}
\usepackage{multirow}

\usepackage{tikz}
\usepackage[
backend=biber,
style=authoryear
]{biblatex}

\addbibresource{refs.bib}

% ===================
% Metropolis BrownU Theme
% https://github.com/vskbellala/metropolis-brown
\usepackage{metropolisbrown}
% ===================

\usepackage{booktabs}
\usepackage[scale=2]{ccicons}

\usepackage{pgfplots}
\usepgfplotslibrary{dateplot}

\usepackage{xspace}
\newcommand{\themename}{\textbf{\textsc{metropolis}}\xspace}

\title{Gut-resident microorganisms and their genes are associated with neuroanatomy and cognitive development}
% \date{\today}
\date{2023-05-17}
\author{\href{https://kevinbonham.com/}{Kevin Bonham, PhD}}
\institute{Biological Sciences | Wellesley College\\https://gitlab.com/kescobo/mbvif202305}
\titlegraphic{\hfill\href{https://wellesley.edu}{\includegraphics[height=1cm]{wellesley_logo_280-eps-converted-to.pdf}}}

\begin{document}

\maketitle


\setbeamertemplate{frame footer}{}
\begin{frame}
    \frametitle{Table of contents}
    \setbeamertemplate{section in toc}[sections numbered]
    \tableofcontents[hideallsubsections]
\end{frame}

\section{Background}

\setbeamertemplate{frame footer}{\cite{koenigSuccessionMicrobialConsortia2011a}}
\begin{frame}
    \frametitle{Gut microbiome development begins at birth and progresses rapidly}
    \centering
    \begin{tikzpicture}
        {\node[inner sep=0pt,anchor=south west] (skin-microbes) at (0,0)
            % Answer: [trim={left bottom right top},clip]
            {\includegraphics[width=0.7\textwidth,trim={0 5.7cm 0 0},clip]{koenig_fig3}};}
        \draw [thick, ->](0,-0.3) -- (3,-0.3) node[anchor=west] {Increasing age}; 
        % \draw[step=0.2,anchor=south west,gray,thin] (0,0) grid (10,6);
        % \draw[step=1.0,anchor=south west] (0,0) grid (10,6);
    \end{tikzpicture}
\end{frame}

\setbeamertemplate{frame footer}{}
\begin{frame}
    \frametitle{Brain development starts before birth and progresses rapidly}
    \centering
    \includegraphics[width=0.7\textwidth]{Brain-development}

\end{frame}

% \setbeamertemplate{frame footer}{\cite{hsiaoMicrobiotaModulateBehavioral2013a,needhamGutderivedMetaboliteAlters2022,sgrittaMechanismsUnderlyingMicrobialMediated2019}}
% \begin{frame}
%     \frametitle{The gut microbiome can affect brain development in mice}
%     \centering
%     \begin{tikzpicture}
%         {\node[inner sep=0pt,anchor=south west] (img1) at (0,0)
%             {\includegraphics[width=0.7\textwidth]{hsiao_5a.png}};}
%         % {\filldraw[white,very thick,anchor=south west] (2.6,0) rectangle (9.6,4.7);}
%         % {\filldraw[white,very thick,anchor=south west] (0,0) rectangle (3.5,5);}
%         % {\filldraw[white,very thick,anchor=south west] (3.65,0) rectangle (9.6,5);}
%         % \draw[step=0.2,anchor=south west,gray,thin] (0,0) grid (9.6,7);
%         % \draw[step=1.0,anchor=south west] (0,0) grid (9.4,6);
%     \end{tikzpicture}
% \end{frame}


% \setbeamertemplate{frame footer}{\cite{liuAlteredGutMicrobiota2019,valles-colomerNeuroactivePotentialHuman2019,sharonHumanGutMicrobiota2019}}
% \begin{frame}
%     \frametitle{The gut microbiome is associated with abnormal brain development in humans}
%     \centering
%     \begin{tikzpicture}
%         {\node[inner sep=0pt,anchor=south west] (img1) at (0,0)
%             {\includegraphics[width=0.45\textwidth]{liu_2c.png}};}
%         {\node[inner sep=0pt,anchor=south west] (img1) at (6.4,0)
%             {\includegraphics[width=0.45\textwidth]{liu_3b.png}};}
%     \end{tikzpicture}
% \end{frame}


\setbeamertemplate{frame footer}{\cite{laiElucidatingHumanGut2022}}
\begin{frame}
    \frametitle{Plausible mechanisms of microbial influence on the brain}
    \centering
        \begin{tikzpicture}
        \node[inner sep=0pt] (img1) at (0,0)
            {\includegraphics[width=0.8\textwidth]{gutbrain-1.png}};
        \node[inner sep=0pt] (img2) at (0,0)
            {\includegraphics[width=0.8\textwidth]{gutbrain-2.png}};
        \node[inner sep=0pt] (img2) at (0,0)
            {\includegraphics[width=0.8\textwidth]{gutbrain-3.png}};
        \pause
        \node[inner sep=0pt] (img2) at (0,0)
            {\includegraphics[width=0.8\textwidth]{gutbrain-4.png}};
        \pause
        \node[inner sep=0pt] (img2) at (0,0)
            {\includegraphics[width=0.8\textwidth]{gutbrain-5.png}};
        \pause
        \node[inner sep=0pt] (img2) at (0,0)
            {\includegraphics[width=0.8\textwidth]{gutbrain-6.png}};
        \pause
        \node[inner sep=0pt] (img2) at (0,0)
            {\includegraphics[width=0.8\textwidth]{gutbrain-7.png}}; 
    \end{tikzpicture}
\end{frame}

\setbeamertemplate{frame footer}{Prints by Alexa Gross, '22}
\begin{frame}
    \frametitle{Open questions}
    \begin{columns}[T,onlytextwidth]
        \column{0.5\textwidth}
        \centering
        \includegraphics[width=0.6\textwidth]{Alexa_brain} \thickspace
    
        \column{0.5\textwidth}
        \centering
        \includegraphics[width=0.6\textwidth]{Alexa_gut}
    \end{columns}
    \begin{alertblock}{}
        \begin{itemize}[<+->]
            \item What role (if any) do microbes play in \textit{typical} human neurodevelopment?
            \item Which microbial species have these roles? (NOTE: this may be location / context specific)
            \item What microbial genes / metabolic activities mediate the effects? 
        \end{itemize}
    \end{alertblock}
\end{frame}

\section[The Cohorts]{Two cohorts of human child development}
\setbeamertemplate{frame footer}{}
\setbeamertemplate{frame footer}{\cite{bonhamGutresidentMicroorganismsTheir2023}}
\begin{frame}
    \frametitle{The RESONANCE cohort of child development - Providence, RI}
    \begin{center}
        \begin{tikzpicture}
            \only<1-3>{\node[inner sep=0pt,anchor=south west] (img1) at (0,0)
                {\includegraphics[width=0.8\textwidth]{Figure1A}};}
            \only<1>{\filldraw[white,anchor=south west] (1.4,0) rectangle (7.9,4.2);}
            \only<1>{\filldraw[white,anchor=south west] (4,0) rectangle (7.4,6);}
            \only<1-2>{\filldraw[white,anchor=south west] (7.9,0) rectangle (11,4);}
            % \draw[step=0.2,anchor=south west,gray,thin] (0,0) grid (11,7);
            % \draw[step=1.0,anchor=south west] (0,0) grid (11,7);
        \end{tikzpicture}
    \end{center}
\end{frame}

% \begin{frame}
%     \frametitle{Subject Demographics in RESONANCE}
%     \centering
%     \begin{table}[!htb]
%         \scriptsize
%         \centering
%         \begin{tabular}{|r|r|r|r|r|}
%           \hline\hline
%           \textbf{group} & \textbf{subgroup} & \textbf{all} \\\hline
%           N subjects &  & 361 \\  \hline
%         \multirow{3}{*}{Samples} & 1 & 262 (72.6\%) \\ \cline{2-3}
%              & 2 & 72 (19.9\%) \\ \cline{2-3}
%              & \textgreater 2 & 27 (7.5\%) \\ \hline
%         \multirow{3}{*}{Age (months)} & min & 2.76 \\ \cline{2-3}
%              & max & 119.3 \\ \cline{2-3}
%              & median & 25.79 \\ \hline
%         \multirow{2}{*}{Sex} & F & 163 (45.2\%) \\   \cline{2-3}
%                              & M & 198 (54.8\%) \\  \hline
%         \multirow{6}{*}{Race} & White & 252 (69.8\%) \\   \cline{2-3}
%             & Black & 40 (11.1\%) \\ \cline{2-3}
%             & Indiginous & 1 (0.3\%) \\ \cline{2-3}
%             & Asian & 7 (1.9\%) \\ \cline{2-3}
%             & Mixed & 48 (13.3\%) \\ \cline{2-3}
%             & Other & 2 (0.6\%) \\ \hline
%         \multirow{6}{*}{Maternal Ed.} & Junior high school & 1 (0.3\%) \\   \cline{2-3}
%             & Some high school & 6 (1.7\%) \\ \cline{2-3}
%             & High school grad & 37 (10.2\%) \\ \cline{2-3}
%             & Some college & 95 (26.3\%) \\ \cline{2-3}
%             & College grad & 90 (24.9\%) \\ \cline{2-3}
%             & Grad/professional school & 123 (34.1\%) \\\hline\hline
%         \end{tabular}
%     \end{table}
% \end{frame}

\setbeamertemplate{frame footer}{}
\begin{frame}
    \frametitle{The Khula cohort of child EF development - South Africa and Malawi}
    \begin{columns}[T,onlytextwidth]
        \column{0.5\textwidth}
            \centering
            \includegraphics[width=0.85\textwidth]{khula_cohort}
            \begin{itemize}
                \item Longitudinal cohorts from birth
                \item Based in Blantyre, Malawi and Capetown, South Africa
            \end{itemize}
        \column{0.5\textwidth}
            \centering
            \includegraphics[width=0.95\textwidth]{khula_loc}
    \end{columns}
\end{frame}

\setbeamertemplate{frame footer}{\cite{bonhamGutresidentMicroorganismsTheir2023}}
\begin{frame}
    \frametitle{Microbial taxa and gene functions are associated with age}
    \centering
    {\includegraphics[width=0.9\textwidth]{Figure1C-F}}
\end{frame}

% \begin{frame}
%     \frametitle{Microbial taxa and gene functions are \textbf{statiscially} associated with age}
%     \begin{center}
%         \begin{tikzpicture}
%             \only<1-2>{\node[inner sep=0pt,anchor=south west] (img1) at (0,0)
%                 {\includegraphics[width=1.0\textwidth]{Figure1G}};}
%             \only<1>{\filldraw[white,anchor=south west] (8,0) rectangle (14,5.8);}
%         \end{tikzpicture}
%     \end{center}
% \end{frame}

\begin{frame}
    \frametitle{Cognitive function can be measured with (mostly) age-invariant assessments}
    \centering
    \includegraphics[width=0.7\textwidth]{Figure1B}
\end{frame}

\section[The results]{Gut microbes and their genes are associated with cognitive development}

\subsection["Traditional" stats approaches]{"Traditional" stats approaches}

\begin{frame}
    \frametitle{Microbial taxa are associated with cognitive function scores}
    \begin{columns}[c,onlytextwidth]
        \column{0.6\textwidth}
        \centering
        \includegraphics[width=0.8\textwidth]{Figure2A}
    
        \column{0.4\textwidth}

        \begin{itemize}[<+->]
            \item Benjamini-Hochberg FDR correction
            \item $q < 0.2$ considered significant
            \item 12 species positively associated, 1 negatively associated
        \end{itemize}
    \end{columns}
    

    $$cogScore \sim bug + age + maternal\_education + read\_depth$$ 

\end{frame}

% \begin{frame}
%     \frametitle{Microbial taxa are associated with cognitive function scores}
%     \centering
%     \begin{tikzpicture}
%         \node[inner sep=0pt,anchor=south west] (img1) at (0,0)
%                 {\includegraphics[width=1.0\textwidth]{Figure2B}};
%         \only<1-3>{\filldraw[white,anchor=south west] (7,0) rectangle (14,5.2);}
%         % \draw[step=0.2,anchor=south west,gray,thin] (0,0) grid (14,5.2);
%         % \draw[step=1.0,anchor=south west] (0,0) grid (14,5.2);
%         \only<2>{\draw[red!60, ultra thick,anchor=west] (5.3,1.4) ellipse (1.5cm and 0.2cm);}
%         \only<3>{\draw[red!60, ultra thick,anchor=west] (5.3,3) ellipse (1.5cm and 0.2cm);}
%         \only<3>{\draw[red!60, ultra thick,anchor=west] (5.3,3.6) ellipse (1.5cm and 0.2cm);}
%         \only<5>{\draw[red!60, ultra thick,->] (1,3.6) -- (1.6,3.6);}
%         \only<6>{\draw[red!60, ultra thick,->] (0.4,4) -- (1,4);}
%     \end{tikzpicture}
%     $$cogScore \sim bug + age + maternal\_education + read\_depth$$ 
% \end{frame}

\setbeamertemplate{frame footer}{\cite{valles-colomerNeuroactivePotentialHuman2019}}
\begin{frame}
    \frametitle{Potentially neuroactive microbial genes are associated with cognitive function scores}

    \begin{tikzpicture}
        \only<1-3>{\node[inner sep=0pt,anchor=south west] (img1) at (0,0)
                {\includegraphics[width=1.0\textwidth]{Figure2D-E}};}   
        {\filldraw[white,anchor=south west] (7,0) rectangle (14,5.2);}
        \only<1-3>\node[draw,draw opacity=0.0,text width=6.5cm,align=left,anchor=north west] at (7,5) {%
                \only<1-3>{\begin{itemize}
                    \item<1-> "\textbf{F}eature \textbf{S}et \textbf{E}nrichment \textbf{A}nalysis"
                    \item<2-> T-scores of all ~2.5M genes are calculated
                    \item<3-> T-scores of in-set genes compared to out-of-set genes using Mann-Whitney U test
                \end{itemize}}
            };
        \only<2>{\draw[red!60, ultra thick,anchor=west] (4,1.1) ellipse (3cm and 0.3cm);}
        \only<3>{\draw[red!60, ultra thick,anchor=west] (3.8,1.1) ellipse (0.1cm and 0.3cm);}
        \only<3>{\draw[red!60, ultra thick,anchor=west] (3.2,1.1) ellipse (0.1cm and 0.3cm);}
        \only<3>{\draw[red!60, ultra thick,anchor=west] (2.7,1.1) ellipse (0.1cm and 0.3cm);}
        % \draw[step=0.2,anchor=south west,gray,thin] (0,0) grid (14,5.2);
        % \draw[step=1.0,anchor=south west] (0,0) grid (14,5.2);
    \end{tikzpicture}

\end{frame}

\begin{frame}
    \frametitle{Potentially neuroactive microbial genes are associated with cognitive function scores}

    \begin{tikzpicture}
        \node[inner sep=0pt,anchor=south west] (img1) at (0,0)
                {\includegraphics[width=1.0\textwidth]{Figure2G}};
        % \only<1-3>{\filldraw[white,anchor=south west] (7,0) rectangle (14,5.2);}
        % \draw[step=0.2,anchor=south west,gray,thin] (0,0) grid (14,3.2);
        % \draw[step=1.0,anchor=south west] (0,0) grid (14,3.2);
    \end{tikzpicture}


\end{frame}

\begin{frame}
    \frametitle{Random forest models are non-linear and transparent}
    \begin{columns}
        \column{0.5\textwidth}
        \begin{itemize}
            \item<1-> Model is aggregation of decision trees
            \item<2-> Whole community can be used as predictor - model learns which features (eg. Microbial species) are important
            \item<3-> Model encodes feature used at each split
                    and how "good" the split was in each tree (increased purity)
            \item<4> Feature "importance" is averaged across trees in the forest
                    (weighted based on number of samples that it splits)
        \end{itemize}

        \column{0.5\textwidth}
        \begin{tikzpicture}
            \node[inner sep=0pt] (img1) at (0,0)
                {\includegraphics[width=0.7\textwidth]{RF-importance-1.png}};
            \only<3-4>{\node[inner sep=0pt] (img2) at (0,0)
                {\includegraphics[width=0.7\textwidth]{RF-importance-2.png}};}
        \end{tikzpicture}

    \end{columns}
\end{frame}

\subsection{Figure 3}

\setbeamertemplate{frame footer}{\cite{bonhamGutresidentMicroorganismsTheir2023}}
\begin{frame}
    \frametitle{RF models can be trained to predict cognitive function scores from microbiome data}

    \begin{table}[!h]
        \footnotesize
        \begin{center}
        \begin{tabular}{|r|r|r|r|r|}
          \hline\hline
          \textbf{Subject Ages (months)} & \textbf{Microbial feature} & \textbf{Demo.} & \textbf{Test set RMSE (± C.I.)} & \textbf{Test set correlation (± C.I.)} \\\hline
          \multirow{5}{*}{0 to 6} & - & + & 13.01 ± 0.05 & -0.14 ± 0.01 \\ \cline{2-5}
                & \multirow{2}{*}{taxa} & - & 12.66 ± 0.05 & \textbf{-0.1 ± 0.01} \\ \cline{3-5}
                &       & + & 12.7 ± 0.05 & -0.12 ± 0.01 \\ \cline{2-5}
                & \multirow{2}{*}{genes} & - & 12.56 ± 0.05 & \textbf{-0.01 ± 0.01} \\ \cline{3-5}
                &       & + & 12.56 ± 0.05 & -0.02 ± 0.01 \\ \cline{1-5}
          \multirow{5}{*}{18 to 120} & - & + & 16.27 ± 0.04 & 0.506 ± 0.003 \\ \cline{2-5}
                & \multirow{2}{*}{taxa} & - & 17.66 ± 0.04 & \textbf{0.363 ± 0.003} \\ \cline{3-5}
                &       & + & 17.29 ± 0.04 & 0.429 ± 0.003 \\ \cline{2-5}
                & \multirow{2}{*}{genes} & - & 18.01 ± 0.04 & \textbf{0.303 ± 0.003} \\ \cline{3-5}
                &       & + & 17.86 ± 0.04 & 0.333 ± 0.004 \\\hline\hline
        \end{tabular}
        \end{center}
    \end{table}

\end{frame}


\begin{frame}
    \frametitle{RF models identify overlapping (but distinct) microbial taxa compared to LMs}
    \begin{tikzpicture}
        \node[inner sep=0pt,anchor=south west] (img1) at (0,0)
                {\includegraphics[width=0.5\textwidth]{Figure3B}};
        \only<2>{\node[inner sep=0pt,anchor=south west] (img1) at (7.5,-0.5)
                {\includegraphics[width=0.4\textwidth]{Figure3D}};}
        % \only<1-3>{\filldraw[white,anchor=south west] (7,0) rectangle (14,5.2);}
        % \draw[step=0.2,anchor=south west,gray,thin] (0,0) grid (14,7);
        % \draw[step=1.0,anchor=south west] (0,0) grid (14,7);
        \only<2>{\draw[tableaupurple, very thick,anchor=south west] (8,5.8) rectangle (12,6);}
        \only<2>{\draw[tableaupurple, very thick,anchor=south west] (8,5) rectangle (12,5.5);}
        \only<2>{\draw[tableaupurple, very thick,anchor=south west] (8,4.1) rectangle (12,4.8);}
        \only<2>{\draw[tableaupurple, very thick,anchor=south west] (8,2.9) rectangle (12,3.3);}
        \only<2>{\draw[tableaupurple, very thick,anchor=south west] (8,0.7) rectangle (12,1.4);}

    \end{tikzpicture}

\end{frame}

% \begin{frame}
%     \frametitle{Important taxa show distinct patterns of prevalence and abundance}

%     \begin{tikzpicture}
%         \only<1-3>{\node[inner sep=0pt,anchor=south west] (img1) at (0,0)
%                 {\includegraphics[width=1.0\textwidth]{Figure3E}};}
%         \only<1>{\filldraw[white,anchor=south west] (0,0) rectangle (3.7,3.5);}
%         \only<1-2>{\filldraw[white,anchor=south west] (3.7,0) rectangle (14,6.8);}
%         % \draw[step=0.2,anchor=south west,gray,thin] (0,0) grid (14,7);
%         % \draw[step=1.0,anchor=south west] (0,0) grid (14,7);
%     \end{tikzpicture}


% \end{frame}

\subsection{Figure 4}

\begin{frame}
    \frametitle{RF models can also predict the size of brain regions}
    \begin{tikzpicture}
        \only<1-2>{\node[inner sep=0pt,anchor=south west] (img1) at (0,0)
                {\includegraphics[width=0.9\textwidth]{Figure4A-B}};}
        \only<1>{\filldraw[white,anchor=south west] (3.7,0) rectangle (12.6,7.4);}
        % \only<3->{\draw[red!60, ultra thick] (7.1,1.4) ellipse (0.2cm and 0.2cm);}
        % \only<3->{\draw[red!60, ultra thick,->] (0.2,1.35) -- (0.6,1.35);}
        % \only<3->{\draw[red!60, ultra thick,->] (6.25,0.3) -- (6.55,0.6);}
        % \only<4->{\draw[tableaupurple, ultra thick] (5.6,3.95) ellipse (0.2cm and 0.3cm);}
        % \only<4->{\draw[tableaupurple, ultra thick,->] (0.7,4) -- (1.1,4);}
        % \only<4->{\draw[tableaupurple, ultra thick,->] (0.7,3.8) -- (1.1,3.8);}
        % \only<4->{\draw[tableaupurple, ultra thick,->] (4.6,0.1) -- (4.9,0.4);}
        % \only<5->{\draw[darkgold, very thick] (0.6,6.7) rectangle (10,7);}
        % \only<5->{\draw[darkgold, ultra thick,->] (4.9,0.1) -- (5.2,0.4);}
        % \only<5->{\draw[darkgold, ultra thick,->] (8.4,0.1) -- (8.7,0.4);}
        % \only<5->{\draw[darkgold, ultra thick,->] (8.8,0.1) -- (9.1,0.4);}
        % \draw[step=0.2,anchor=south west,gray,thin] (0,0) grid (12.6,7.4);
        % \draw[step=1.0,anchor=south west] (0,0) grid (12.6,7.4);

   \end{tikzpicture}
\end{frame}

\setbeamertemplate{frame footer}{}
\begin{frame}{Summary}

\pause
\begin{itemize}[<+->]
    \item Gut microbial taxa and gene functions are associated with normal cognitive development
    \item Random forest models provide a unique way to investigate important taxa in high-dimensional microbiome data
    \item RFs enable uncovering relationships between correlated features
\end{itemize}

\end{frame}


\begin{frame}[standout]
    \frametitle{Thank You!}
    \begin{columns}
        \column{0.6\textwidth}
            \includegraphics[width=0.95\textwidth]{lab-photo}
        \column{0.4\textwidth}
            \small
            \textbf{Klepac-Ceraj Lab}
            \begin{itemize}
                \item Vanja
                \item Guilherme Fahur Bottino
                \item Shelley Hoeft McCann
                \item Danielle Peterson ('20)
                \item Lauren Tso ('20)
                \item Anika Luo ('23)
                \item Deniz Uzun ('23)
                \item So many other undergrads!
            \end{itemize}

    \end{columns}
    https://gitlab.com/kescobo/mbvif202305
\end{frame}

\setbeamertemplate{frame footer}{}
\begin{frame}[allowframebreaks]{References}

\printbibliography[heading=none]

\end{frame}

% %----------------
% % Slide examples
% %----------------

% \appendix

% \section{Elements}

% \begin{frame}[fragile]{Typography}
%       \begin{verbatim}The theme provides sensible defaults to
% \emph{emphasize} text, \alert{accent} parts
% or show \textbf{bold} results.\end{verbatim}

%   \begin{center}becomes\end{center}

%   The theme provides sensible defaults to \emph{emphasize} text,
%   \alert{accent} parts or show \textbf{bold} results.
% \end{frame}

% \begin{frame}{Font feature test}
%   \begin{itemize}
%     \item Regular
%     \item \textit{Italic}
%     \item \textsc{SmallCaps}
%     \item \textbf{Bold}
%     \item \textbf{\textit{Bold Italic}}
%     \item \textbf{\textsc{Bold SmallCaps}}
%     \item \texttt{Monospace}
%     \item \texttt{\textit{Monospace Italic}}
%     \item \texttt{\textbf{Monospace Bold}}
%     \item \texttt{\textbf{\textit{Monospace Bold Italic}}}
%   \end{itemize}
% \end{frame}

% \begin{frame}{Lists}
%   \begin{columns}[T,onlytextwidth]
%     \column{0.33\textwidth}
%       Items
%       \begin{itemize}
%         \item Milk \item Eggs \item Potatos
%       \end{itemize}

%     \column{0.33\textwidth}
%       Enumerations
%       \begin{enumerate}
%         \item First, \item Second and \item Last.
%       \end{enumerate}

%     \column{0.33\textwidth}
%       Descriptions
%       \begin{description}
%         \item[PowerPoint] Meeh. \item[Beamer] Yeeeha.
%       \end{description}
%   \end{columns}
% \end{frame}
% \begin{frame}{Animation}
%   \begin{itemize}[<+- | alert@+>]
%     \item \alert<4>{This is\only<4>{ \textbf{really}} important}
%     \item Now this
%     \item And now this
%   \end{itemize}
% \end{frame}

% {%
% \setbeamertemplate{frame footer}{My custom footer}
% \begin{frame}[fragile]{Frame footer}
%     \themename defines a custom beamer template to add a text to the footer. It can be set via
%     \begin{verbatim}\setbeamertemplate{frame footer}{My custom footer}\end{verbatim}
% \end{frame}
% }

% \begin{frame}{References}
%   Some references to showcase [allowframebreaks] \cite{knuth92,ConcreteMath,Simpson,Er01,greenwade93}
% \end{frame}

% \section{Conclusion}

% \begin{frame}{Summary}

%   Get the source of this theme and the demo presentation from

%   \begin{center}\url{github.com/matze/mtheme}\end{center}

%   The theme \emph{itself} is licensed under a
%   \href{http://creativecommons.org/licenses/by-sa/4.0/}{Creative Commons
%   Attribution-ShareAlike 4.0 International License}.

%   \begin{center}\ccbysa\end{center}

% \end{frame}

% {%\setbeamercolor{palette primary}{fg=black, bg=yellow}
% \begin{frame}[standout]
%   Questions?
% \end{frame}
% }


% \begin{frame}[fragile]{Backup slides}
%   Sometimes, it is useful to add slides at the end of your presentation to
%   refer to during audience questions.

%   The best way to do this is to include the \verb|appendixnumberbeamer|
%   package in your preamble and call \verb|\appendix| before your backup slides.

%   \themename will automatically turn off slide numbering and progress bars for
%   slides in the appendix.
% \end{frame}

% \begin{frame}[allowframebreaks]{References}

%   \bibliography{demo}
%   \bibliographystyle{abbrv}

% \end{frame}

\end{document}
